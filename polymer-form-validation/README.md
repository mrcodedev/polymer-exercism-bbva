# Polymer - From Validation
Using [Polymer 2.x](https://www.polymer-project.org/)

First create a ```src``` folder and put a ```form-validation.html``` file. This contains all in one:
- style
- html
- js

The form have these fields:

- **Input text**: Name named *formName*.
- **Input text**: Email named *formEmail*.
- **Select**:  Number childen named *formChildren*.


And these validations:

- **Name**: Not empty and only latin characters.
- **Email**: Valid email format.
- **Number Children**: Minium of 3 amount.

And if the form is invalid must show a help text, with a "error" className.

The are three properties:
- **errorName**: for the error name string.
- **errorEmail**: for the error email string.
- **errorChildren**: for the error children string.




## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.