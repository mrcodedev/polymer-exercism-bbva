# Polymer - polymer-comunication-event
Using [Polymer 2.x](https://www.polymer-project.org/)

First create both files in ```src``` folder and put that ```polymer-comunication-event.html``` and that ```send-event.html``` files. 


The file ```send-event.html``` has a button that fires an event, whith **detail** object with a **value** property name and **"from two to six"** property value. 

When fires this event named **```change-value```** the parent component(who has inside the ```send-event.html``` component) ```polymer-comunication-event.html``` change the ```h1``` innerHTML with the ```detail.value``` of the event.

## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.