# Polymer - From Validation

Using [Polymer 2.x](https://www.polymer-project.org/)

First create a `src` folder and put a `element-dom-html.html` file. This contains all in one:

- style
- html
- js

This exercise is to manipulate the dom, to append in a `<p>`raw html by a property typed as String.

```
htmlContent: {
  type: String,
  value: '<span>Demo for html content</span>'
}
```

## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```

## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.
