# Polymer - From Validation
Using [Polymer 2.x](https://www.polymer-project.org/)

First create a ```src``` folder and put a ```computed-demo.html``` file. This contains all in one:
- style
- html
- js

In this exercise you must use a **computed** to contruct a Euro currency with a value given.
The currency must be present as spaninsh connotations using dots for the miles and commas for the decimals.


## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.