# Polymer - From Validation
Using [Polymer 2.x](https://www.polymer-project.org/)

First create a ```src``` folder and put a ```work-arrays.html``` file. This contains the property **elements** typed as Array. And the list bellow methods:

- **addElement**: Push a item in *elements*.
- **removeElement**:  Given an value find in *elements* and remove it.
- **getElementByIndex**: Given the index return the value in that position of *elements*.
- **removeLastElement**: Remove the last item of *elements*.
- **addElementsAtBegin**: Given an array of elements, append at he begin of *element*.


## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.