# Polymer - From Validation

Using [Polymer 2.x](https://www.polymer-project.org/)

First create a `src` folder and put theses two files:

- `mixin-polymer.html`: this is the mixin in Polymer version 1.x
- `mixin-hybrid.html`: this is the component that load the mixin `mixin-polymer`.

The mixin got the **demo** Boolean property that is default to false, a metod **changeDemo** that updates **demo** prop with the given value, and a method **changeParentProp** that update the component property **parentProp** with the given value.

## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```

## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.
