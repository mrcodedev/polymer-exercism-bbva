# Polymer - From Validation
Using [Polymer 2.x](https://www.polymer-project.org/)

First create a ```src``` folder and put a ```life-cycle-disconnected-callback.html``` file. 

The idea is a validate when component when is removed calls **disconnectedCallback** AUTOMATIC. So create a method *isDisconnectedCallback* that mades it.

The mehotd to known is called method **disconnectedCallback**, is to create a prop **deleted** with a default value to **false** and turn to **true** in in the class methid **disconnectedCallback** in own class. In this case the hook of delete is correct.

## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.