# Polymer - From Validation
Using [Polymer 2.x](https://www.polymer-project.org/)

First create a ```src``` folder and put a ```element-dom-repeat.html``` file. This contains all in one:
- style
- html
- js

This exercise is to manipulate the dom for repeat DOMElements, in this case  ```<li>```, by the **elems** array property, to append or remove the elements, using the Polymer.Element extended class.

Create a list formed by ul/li elemenets to repeat the property **elems** array.
```
<ul>
<li>elem[i]</li>
</ul>
```



## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install -g bower
$ npm install -g polymer-cli
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.